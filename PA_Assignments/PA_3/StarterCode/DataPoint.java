// TODO: Add the appropriate file header comment here.
///////////////////////////////////////////////////////////////////////////////
//                   ALL STUDENTS COMPLETE THESE SECTIONS
// Main Class File:    (name of main application class)
// File:               (DataPoint.java)
// Quarter:            (course) Winter 2022
//
// Author:             (rthavixay@ucsd.edu)
// Instructor's Name:  (Prof. Greg Miranda)
//
//////////////////// PAIR PROGRAMMERS COMPLETE THIS SECTION ///////////////////
// Pair Partner:       (name of your pair programming partner)
// Email:              (email address of your programming partner)
// Instructor's Name:  (name of your partner's instructor)
//
//////////////////// STUDENTS WHO GET HELP FROM OTHER THAN THEIR PARTNER //////
//                   fully acknowledge and credit all sources of help,
//                   other than Instructors and TAs.
//
// Persons:          Identify persons by name, relationship to you, and email.
//                   Describe in detail the the ideas and help they provided.
//
// Online sources:   Avoid web searches to solve your problems, but if you do
//                   search, be sure to include Web URLs and description of
//                   of any information you find.
//////////////////////////// 80 columns wide //////////////////////////////////

// TODO: Add the appropriate class header comment here.
public class DataPoint {
    // The number and races represented in this data point.  DO NOT CHANGE.
    public int numRaces = 8;
    private String[] races = {"White", "Black", "LatinX", "Asian", "AIAN",
                              "NHPI", "Multiracial", "Other"};
    // TODO: Add private member variables here.
    
    private String date;
    private String state;
    private int totalCases;
    private int[] casesByRace;



    // TODO: Add comment and implement
    public DataPoint(String dateIn, String stateIn, int totalCasesIn, int[] casesByRaceIn)
    {
        // TODO

        this.date = dateIn;
        this.state = stateIn;
        this.totalCases = totalCasesIn;
        this.casesByRace = casesByRaceIn;

        
     }

    // TODO: Add comment and implement.
    public String getDate() {
        // TODO: change the line below
        return this.date;
    }

    // TODO: Add comment and implement.
    public String getState() {
        // TODO: change the line below
        return this.state;
    }

    // TODO: Add comment and implement.
    public int getCasesByRace(int raceIndex) {
        // TODO: Change the line below and add more code.

        return this.casesByRace[raceIndex];
    }

    // TODO: Add comment and implement.
    public int getTotalCases() {
        // TODO: Change the line below
        return this.totalCases;
    }

    /**
    * Return the race name associated with the given index in this data point.
    * Preconditions: index is between 0 (inclusive) and numRaces (8) (exclusive)
    *
    * DO NOT CHANGE.
    *
    * @param index The index of the race.
    * @return The name of the race (e.g. "White" or "LatinX")
    */
    public String getRaceName(int index)
    {
        return this.races[index];
    }
}
