class StringCalculator {
    private String[] data;  // The data points to use in the calculations

    /**
    * Create a new StringCalculator object
    */
    public StringCalculator(String[] input)
    {
        // Create a new String array of the proper size
        //this.data = null;

        //Shallow Copy
       this.data = input;

        //Loop through and copy the entries from input to data (Deep Copy)
        // this.data = new String[input.length];
        // for (int i =0; i < input.length; i++)
        // {
        //     //this.data[i] = input[i];
        //     this.data[i] = new String(input[i]);
        // }

    }

    /**
    * Print the strings stored in this object.
    */
    public void printStrings()
    {
        // Print the strings one at a time
        for(String item: this.data)
        {
            System.out.print(item + " ");
        }

    }

    /**
    * Return the number of times the string item is stored in this object
    *
    * @param item The string to detect
    * @return The number of times item appears in this.data
    */
    public int countOccurances(String item)
    {
        int countOcc = 0;
        for(String dataPoint: this.data)
        {
            if (dataPoint.equals(item))
            {
                countOcc += 1;
            }
        }
        return countOcc;
    }

    // Notice that we can put our tester code in the same class in the
    // main method if we want.
    public static void main(String[] args)
    {
        String[] test = {"CA", "NH", "WI", "NH"};
        StringCalculator calculator = new StringCalculator(test);

        //calculator.printStrings();

        test[3] = "WA";

        calculator.printStrings();

        // int count = calculator.countOccurances("NH");
        // System.out.println(count);
    }
}
