// TODO: Add file header comment

import java.io.IOException;
import java.util.Scanner;
import java.io.File;


// TODO: Add class header comment
public class PA4 {

    // TODO: Add comments and implement this method
    public MazePoint[][] readMaze(String fileToRead) throws IOException
    {
        // TODO: Replace this
        MazePoint[][] maze = new MazePoint[0][0];


        return maze;
    }

    // TODO: Add comments and implement this method
    public void escapeFromMaze(MazePoint [][] maze){
        //TODO
    }

    /**
    * Print the maze as a 2D grid.  You should call this method from
    * testRead and testEscape, as well as from main.
    *
    * Precondition: The maze is not null.
    * Postcondition: The maze has been printed and is unmodified.
    *
    * @param maze The maze to be printed.
    */
    private void printMaze(MazePoint[][] maze)
    {
        // TODO: Implement this method
    }

    /**
    * Compare two maze arrays.  Return true if they have .  You should call
    * this method from testRead and testEscape.
    *
    * Precondition: The mazes are not null and have the same size.
    * Postcondition: The mazes are not modified.
    *
    * @param maze1 the first maze to compare.
    * @param maze2 the second maze to compare.
    * @return true if the mazes contain all the same symbols and false otherwise
    */
    private boolean mazeMatch(MazePoint[][] maze1, MazePoint[][] maze2)
    {
        // TODO: Implement this method
        return false;
    }

    // TODO: Add comments and implement method
    public boolean testRead(String fileToRead, MazePoint[][] expected) throws
                            IOException
    {
        return false;
    }

    // TODO: Add comments and implement method
    public boolean testEscape(MazePoint[][] maze,
                              MazePoint[][] expectedSolution)
    {
        return false;
    }

    /**
    * Run unit tests.  You will add to this method.
    * Prints a message indicating whether all tests passed or failed.
    * The method will abort on the first failed test.
    * @return true if all unit tests pass.  false if at least one test fails.
    */
    public boolean unitTests() throws IOException {
        // Manually create the expected maze to match file input1
        MazePoint[][] expected = new MazePoint[3][3];
        expected[0][0] = new MazePoint(false);
        expected[0][1] = new MazePoint(false);
        expected[0][2] = new MazePoint(true);
        expected[1][0] = new MazePoint(true);
        expected[1][1] = new MazePoint(false);
        expected[1][2] = new MazePoint(false);
        expected[2][0] = new MazePoint(true);
        expected[2][1] = new MazePoint(true);
        expected[2][2] = new MazePoint(false);

        if (!this.testRead("input1", expected)) {
            System.out.println("Read test 1 failed.");
            return false;
        }
        else {
          System.out.println("Read test 1 passed!");
        }
        //TODO: Add at least two more calls to testRead

        // At this point readMaze is working, so we can use it.
        MazePoint[][] maze1 = this.readMaze("input1");

        // Modify the expected maze from above to have the path
        expected[0][0].setToPath();
        expected[0][1].setToPath();
        expected[1][1].setToPath();
        expected[1][2].setToPath();
        expected[2][2].setToPath();

        if (!this.testEscape(maze1, expected)) {
            System.out.println("Escape test 1 failed.");
            return false;
        }
        else {
          System.out.println("Escape test 1 passed!");
        }

        //TODO: Add at least three more calls to testMaze

        return true;
    }


    // TODO: Add comments and implement method.
    public static void main(String[] args) throws IOException
    {
        PA4 solver = new PA4();

        // Perform unitTest first
        if(solver.unitTests()) {
            System.out.println("All unit tests passed.\n");
        } else {
            System.out.println("Failed test.\n");
            return;
        }

        //TODO: Part 3

    }
}
