// Code from ZyBooks, expanded for class.

import java.util.Scanner;
import java.io.IOException;
import java.io.FileInputStream;


public class ModifyArray {

    /* Display array values */
    public void displayValues(int [] arrayVals) {
        int i;

        for (i = 0; i < arrayVals.length; ++i) {
            System.out.print(arrayVals[i] + " ");
        }
        System.out.println("");
    }

    public void displayValues(char [][] arrayVals) {
        int i;
        int j;

        for (i = 0; i < arrayVals.length; ++i) {
            for(j = 0; j < arrayVals[i].length; j++) {
                System.out.print(arrayVals[i][j] + " ");
            }
            System.out.println("");
        }
        System.out.println("");
    }

    /**
    * Modify the elements along the diagonal of an array of chars.
    * Set them equal to '^'.
    * If the array is not square, then traverse the bottom
    * row or rightmost column until reaching the bottom right.
    * Examples:
    *  @ @ @      ^ @ @       @ @ @ @ @     ^ @ @ @ @
    *  @ @ @  --> @ ^ @       @ @ @ @ @ --> @ ^ @ @ @
    *  @ @ @      @ @ ^       @ @ @ @ @     @ @ ^ ^ ^
    *  @ @ @      @ @ ^
    *      (3, 2) 4 rows, 3 cols
    */
    public void diagonalPath(char[][] theArray)
    {

        //Handle edge cases
        if (theArray == null)
        {
            // Print a user- friendly message
            System.out.println("Null 2D array..");
            // Stop the execution of the function
            return;
        }

        if(theArray.length == 0)
        {
            // Print a user- friendly message
            System.out.println("Number of rows is zero.");
            // Stop the execution of the function
            return;
        }

        //Goal is to traverse from upper left corner to lower right corner
        int row = 0;
        int col = 0;




        while(theArray[nRows-1][nCols-1] != "^")
        {
            theArray[row][col] = '^';
            if (row < nRows -1)
            {
                row ++;
            }
            if 

        }

        
    }



    /**
    * Test method for the diagonalPath method.
    *
    * @param theArray The array to input to diagonalPath
    * @paramm expected The array that is expected after running diagonalPath
    *
    * @return true if diagonalPath produces the correct output and false
    *   otherwise.
    */
    public boolean testDiagonalPath(char[][] theArray, char[][] expected) {


        //Step 1 - Run the method I would like to test
        this.diagonalPath(theArray);

        //Step 2 - Compare the Array and expected
        for (int r = 0; r <theArray.length; r++)
        {
            for (int c = 0; c < theArray[r].length; c++)
            {
                if (theArray[r][c] != expected[r][c])
                return false;
            }
        }

        return true;

        return true;
    }


    /**
    * Read the Strings from the file filename into a one dimensional
    * array of Strings.  Strings in the file will be separated by
    * spaces or line breaks.  The first line in the file specifies
    * the number of Strings in the file.
    * For example:
    *    9
    *    % % % #
    *    @ @
    *    S S 6
    *
    * should be read into the array:
    * ["%", "%", "%", "#", "@", "@", "S", "S", "6"]
    */
    public String[] readArrayFromFile(String filename) throws IOException
    {
        return null;
    }

    /**
    *  Swap every other element in the array.  i.e. if the array
    * is [1, 2, 3, 4] before the method, it will be [2, 1, 4, 3] after.
    * The swap happens in place.
    *
    * Precondition: The length of the array is even.
    * Postcondition: The array is modified as above.
    *
    * @param theArray The array to modify.  Its length must be even.
    */
    public void swapEveryOther(int[] theArray)
    {
        
    }

    /**
    *  Find and print the largest and the smallest element from the given
    *   2D array
    *
    * Precondition: The array has at least one element, and is rectangular.
    * Postcondition: The array is unmodified.
    *
    * @param theArray The array to process.
    */
    public void printBiggestAndSmallest(int[][] theArray)
    {
        // What will happen if the array is empty?
        // How could we handle that more gracefully?
        int largest = theArray[0][0];
        int smallest = theArray[0][0];
		for (int r = 0; [BLANK1] ; r++) {  // COMPLETE
			for (int c = 0; [BLANK2] ; c++) {  // COMPLETE
				if ( theArray[r][c] [BLANK3]) {  
					largest = theArray[r][c];
				}
				if ( [BLANK4] ) {  
					smallest = theArray[r][c];
				}
			}
		}

        System.out.println("Largest: " + largest);
        System.out.println("Smallest: " + smallest);
    }

    public static void main (String [] args) throws IOException
    {
        int numElem = 4;
        int[] sorted = new int[numElem];
        ModifyArray numInverter = new ModifyArray();
        // Add values to the array
        sorted[0] = 10;
        sorted[1] = 20;
        sorted[2] = 30;
        sorted[3] = 40;
       // sorted[4] = 50;

        numInverter.swapEveryOther(sorted);
        numInverter.displayValues(sorted);



        // int[][] twoDArray = new int[2][3];

        // twoDArray[0][0] = 9;
        // twoDArray[0][1] = 1;
        // twoDArray[0][2] = 77;
        // twoDArray[1][0] = -2;
        // twoDArray[1][1] = 101;
        // twoDArray[1][2] = 34;

        //int[][] twoDArray = new int[1][0];
       // numInverter.printBiggestAndSmallest(twoDArray);

         String[] fromFile = numInverter.readArrayFromFile("arrayfile");
         for (String s : fromFile) {
             System.out.print(s + " ");
         }
         System.out.println();



    }
}
