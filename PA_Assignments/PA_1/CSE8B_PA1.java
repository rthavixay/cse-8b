///////////////////////////////////////////////////////////////////////////////
//                   ALL STUDENTS COMPLETE THESE SECTIONS
// Title:              Building and Testing Classes
// Files:              java, javac
// Quarter:            Winter 2022
//
// Author:             Roger Thavixay              
// Email:              rthavixay@ucsd.edu
// Instructor's Name:  Professor Greg Miranda
//
//////////////////// PAIR PROGRAMMERS COMPLETE THIS SECTION ///////////////////
//
//                  CHECK ASSIGNMENT PAGE TO see IF PAIR-PROGRAMMING IS ALLOWED
//                  If pair programming is allowed:
//                  1. Read PAIR-PROGRAMMING policy
//                  2. Choose a partner wisely
//                  3. Complete this section for each program file
//
// Pair Partner:        N/A
// Email:               N/A
// Instructors's Name:  N/A
// Lab Section:         N/A
//
//////////////////// STUDENTS WHO GET HELP FROM OTHER THAN THEIR PARTNER //////
//                   must fully acknowledge and credit those sources of help.
//                   Instructors and TAs do not have to be credited here,
//                   but roommates, relatives, strangers, etc do.
//
// Persons:          N/A
//
// Online sources:   N/A
//////////////////////////// 80 columns wide //////////////////////////////////


 //Import Scanner class
import java.util.Scanner;

public class CSE8B_PA1{
    public static void main(String[] args)  {

        // Score Keeping for User
        int score = 0;

        //Create a Scanner  that will take the user input
        Scanner input = new Scanner(System.in);


        System.out.println("Welcome to the CSE 8B trivia quiz. I have three questions for you about a female computer scientist named  Jeannette M. Wing");
        
        // Created a string variable that is asking the user the first questions
        String q1 = "1. Which University was Jeannetter Wing a computer science professor at?  (Type A, B, C, or D)\n "  +
        "(A)Standford University \n (B) Columbia University \n (C) UC San Diego \n (D) Some Community College";



        //Prints the Question 1 statement
        System.out.println(q1);
        
        // Takes Users input and uppercase it   "Reuse Scanner"
        String userQ1Input = input.nextLine().toUpperCase();
        
        // Use .equals("") for strings but == will be good for int,long,shorts,floats etc
        if (userQ1Input.equals("B")){
            score += 1;
            System.out.println("You are Correct!");
        }
            else{
                System.out.println("You are incorrect :(");
                System.out.println("The answer is 'B', Columbia University");
        }
         System.out.println("Your score: " + score);



         // Created a string variable that is asking the user the first questions
         String q2 = "2. Which tech company was she the Corporate Vice President of?";

         //Prints the Question 1 statement
         System.out.println(q2);

         // Takes Users input and uppercase it   "Reuse Scanner"
        String userQ2Input = input.nextLine().toUpperCase();


        if (userQ2Input.equals("MICROSOFT")){     
            score += 1;
            System.out.println("You are Correct!");

        }

            else if (userQ2Input.equals("MICROSOFT REASEARCH CONNECTIONS")){
            score += 1;
            System.out.println("You are Correct!");

            }

            else{
                System.out.println("You are incorrect :(");

                System.out.println("Jeanette was a Corporate Vice President at the Microsoft Research Connections.");

            }

         System.out.println("Your score: " + score);


         // Created a string variable that is asking the user the first questions
         String q3 = "3. Where did Jeannetter earn her Computer Science Degree at?";

         //Prints the Question 1 statement
         
         System.out.println(q3);

         // Takes Users input and uppercase it   "Reuse Scanner"
        String userQ3Input = input.nextLine().toUpperCase();

        


        if (userQ3Input.equals("MIT")){
            score += 1;
            System.out.println("You are Correct!");
        }
        
            else if (userQ3Input.equals("MASSACHUSETTS INSTITUTE OF TECHNOLOGY")){
                score += 1;
                System.out.println("You are Correct!");
            }
            
            else if (userQ3Input.equals("M.I.T.")){
                score += 1;
                System.out.println("You are Correct!");
            }

            else{

                System.out.println("You are incorrect :(");

                System.out.println(" The correct answer is MIT. Jeannette Wing earned her Computer Science degree in jun 1979 and also earned her Computer Science PhD at MIT under John Guttag in 1983 ");

            }


    

        System.out.println("Your final score is  " + score);
        
        input.close();
    }
}