import java.util.Scanner;

public class ElectionTester{


      // String[] args means an array of sequence of (strings) are passed to the "Main"function
      public static void main(String args[]) {
        //Store the Variables 


        Candidate Candidate1 = new Candidate("Roger Thavixay","UCSD" ); //Create Object of Class Candidate
        Candidate Candidate2 = new Candidate("Roddie Rich","SDSU" ); //Create Object of Class Candidate

        System.out.println(Candidate1.getName() + ", " + Candidate1.getParty() + ", " + Candidate1.getVotes());
        System.out.println(Candidate2.getName() + ", " + Candidate2.getParty() + ", " + Candidate2.getVotes());
        Candidate2.setParty("UCSD");
        Candidate2.incrementVotes();
        Candidate2.incrementVotes();
        System.out.println(Candidate2.getName() + ", " + Candidate2.getParty() + ", " + Candidate2.getVotes());


        Election election = new Election();
        Scanner input = new Scanner(System.in);
        //when I pass in input into a method it is called argument

        
        election.runElection(input);   //input here is the argument of the method runElection
        



        //Remember: You are unable to see Private member variables in Candidate Class File

      }
    
}