///////////////////////////////////////////////////////////////////////////////
//                   ALL STUDENTS COMPLETE THESE SECTIONS
// Title:              Building and Testing Classes
// Files:              Candidate.java , ElectionTester.java
// Quarter:            Winter 2022
//
// Author:             Roger Thavixay              
// Email:              rthavixay@ucsd.edu
// Instructor's Name:  Professor Greg Miranda
//
//////////////////// PAIR PROGRAMMERS COMPLETE THIS SECTION ///////////////////
//
//                  CHECK ASSIGNMENT PAGE TO see IF PAIR-PROGRAMMING IS ALLOWED
//                  If pair programming is allowed:
//                  1. Read PAIR-PROGRAMMING policy
//                  2. Choose a partner wisely
//                  3. Complete this section for each program file
//
// Pair Partner:        N/A
// Email:               N/A
// Instructors's Name:  N/A
// Lab Section:         N/A
//
//////////////////// STUDENTS WHO GET HELP FROM OTHER THAN THEIR PARTNER //////
//                   must fully acknowledge and credit those sources of help.
//                   Instructors and TAs do not have to be credited here,
//                   but roommates, relatives, strangers, etc do.
//
// Persons:          N/A
//
// Online sources:   N/A
//////////////////////////// 80 columns wide //////////////////////////////////

// Note this is a seperate class
public class Candidate 
{

      //This is only Available for the Candidate Class
    private String name;
    private String party;
    private int voteCount;





      // Constructor:

      public Candidate(String candidateName, String candidateParty) 
      {
        this.name = candidateName;
        this.party = candidateParty;
        this.voteCount = 0;
      }

      public String getName()
      {
        return this.name;
      }

      public String getParty()
      {
        return this.party;
      }

      public int getVotes()
      {
        return voteCount;
      }

      public void setParty(String newParty)
      {
        this.party = newParty;
      }

      public void incrementVotes()
      {
        this.voteCount += 1;
      }

}