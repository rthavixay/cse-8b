///////////////////////////////////////////////////////////////////////////////
//                   ALL STUDENTS COMPLETE THESE SECTIONS
// Title:              Building and Testing Classes
// Files:              Candidate.java , ElectionTester.java
// Quarter:            Winter 2022
//
// Author:             Roger Thavixay              
// Email:              rthavixay@ucsd.edu
// Instructor's Name:  Professor Greg Miranda
//
//////////////////// PAIR PROGRAMMERS COMPLETE THIS SECTION ///////////////////
//
//                  CHECK ASSIGNMENT PAGE TO see IF PAIR-PROGRAMMING IS ALLOWED
//                  If pair programming is allowed:
//                  1. Read PAIR-PROGRAMMING policy
//                  2. Choose a partner wisely
//                  3. Complete this section for each program file
//
// Pair Partner:        N/A
// Email:               N/A
// Instructors's Name:  N/A
// Lab Section:         N/A
//
//////////////////// STUDENTS WHO GET HELP FROM OTHER THAN THEIR PARTNER //////
//                   must fully acknowledge and credit those sources of help.
//                   Instructors and TAs do not have to be credited here,
//                   but roommates, relatives, strangers, etc do.
//
// Persons:          N/A
//
// Online sources:   N/A
//////////////////////////// 80 columns wide //////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//                   ALL STUDENTS COMPLETE THESE SECTIONS
// Main Class File:    (ElectionTester.java)
// File:               (Election.java)
// Quarter:            (course) Winter 2022
//
// Author:             (rthavixay@ucsd.edu)
// Instructor's Name:  (Professor Greg Miranda)
//
//////////////////// PAIR PROGRAMMERS COMPLETE THIS SECTION ///////////////////
// Pair Partner:       (N/A)
// Email:              (N/A
// Instructor's Name:  (N/A)
//
//////////////////// STUDENTS WHO GET HELP FROM OTHER THAN THEIR PARTNER //////
//                   fully acknowledge and credit all sources of help,
//                   other than Instructors and TAs.
//
// Persons:          N/A
//                   
//
// Online sources:   N/A
//                   
//                   
//////////////////////////// 80 columns wide //////////////////////////////////
import java.util.Scanner;

public class Election
{
    //Can only be called within this class
    private Candidate candidate3;
    private Candidate candidate4;
    private Candidate candidate5;
    
    public Election()
    {
        candidate3 = new Candidate("Elon Musk", "Tesla");
        candidate4 = new Candidate("Steve Jobs", "Apple");
        candidate5 = new Candidate("Jeff Bezos", "Amazon");

        



    }


        public void runElection(Scanner Scan)
        
        {
        // input is passed as a parameter of runElection(Scanner Scan)

        
        System.out.println("Welcome to the election. We have 3 candidates:\n1. " + candidate3.getName() + " from " + candidate3.getParty());
        System.out.println("2. " + candidate4.getName() + " from " + candidate4.getParty());
        System.out.println("3. " + candidate5.getName() + " from " + candidate5.getParty());
        System.out.println("Enter the next vote (by candidate number). Enter -1 if there are no more votes. ");


        int userInput = Scan.nextInt();

        while ( userInput != -1) //While input is not -1 program will keep executing
        {
            if (userInput  == 1){
                candidate3.incrementVotes();
            }
            if (userInput == 2){
                candidate4.incrementVotes();
            }
            if (userInput == 3){
                candidate5.incrementVotes();

            }
            System.out.println("Enter the next vote (by candidate number). Enter -1 if there are no more votes. ");
            userInput = Scan.nextInt(); // Prompts the user again

        }


        

            System.out.println("The vote counts as follows:");
            System.out.println(candidate3.getName() + ": " + candidate3.getVotes());
            System.out.println(candidate4.getName() + ": " + candidate4.getVotes());
            System.out.println(candidate5.getName() + ": " + candidate5.getVotes());
            

            


            // Below this comment is finding the winner or tie for the voting poll.
            if (candidate3.getVotes() > candidate4.getVotes() && candidate3.getVotes() > candidate5.getVotes()) //Check to see if candidate 3 has most votes
            
            {
                System.out.println("The winner is: " + candidate3.getName() + "!");
            } 
            if (candidate4.getVotes() > candidate3.getVotes() && candidate4.getVotes() > candidate5.getVotes()) //Check to see if candidate 4 is the winner
            
            {
                System.out.println("The winner is: " + candidate4.getName() + "!");
            }
            if (candidate5.getVotes() > candidate4.getVotes() && candidate5.getVotes() > candidate3.getVotes()) // Check to see if candidate 5 is the winner
            
            {
                System.out.println("The winner is: " + candidate5.getName() + "!");
            }
            if (candidate3.getVotes() == candidate4.getVotes() && candidate3.getVotes() == candidate5.getVotes() && candidate4.getVotes() == candidate5.getVotes())// Check to see if tie between all candidates
            
            {
                System.out.println("There is a tie between " + candidate3.getName() + " and " + candidate4.getName() + " and " + candidate5.getName() + "!");
                return;
            }
            
            if (candidate3.getVotes() == candidate4.getVotes() && candidate3.getVotes() > candidate5.getVotes()) // Check if tie between candidate 3 and 4
            
            {
                System.out.println("There is a tie between " + candidate3.getName() + " and " + candidate4.getName() + "!"); 
            }
            if (candidate4.getVotes() == candidate5.getVotes() && candidate4.getVotes() > candidate3.getVotes())  // check if there is a tie between candidate 4 and 5
            
            {
                System.out.println("There is a tie between " + candidate4.getName() + " and " + candidate5.getName() + "!"); 
            }
            if (candidate5.getVotes() == candidate3.getVotes() && candidate5.getVotes() > candidate4.getVotes()) // check if there is a tie between candidate 5 and 3
            
            {
                System.out.println("There is a tie between " + candidate5.getName() + " and " + candidate3.getName() + "!"); 
            }

            Scan.close();


            }

     

         








;






                   





            
        

            

            
                
            }
            
            
            
            

    


        


    



